import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'

import { LazyRoutingModule } from './lazy-routing.module';
import { LazyComponent } from './lazy.component';
import { LazyPageComponent } from './lazy-page/lazy-page.component';
import { TableComponent } from './table/table.component';

@NgModule({
  imports: [
    SharedModule,
    LazyRoutingModule
  ],
  declarations: [LazyComponent, LazyPageComponent, TableComponent]
})
export class LazyModule { }
