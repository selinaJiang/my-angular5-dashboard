import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LazyComponent } from './lazy.component';
import { LazyPageComponent } from './lazy-page/lazy-page.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  {
    path: '',
    component: LazyComponent,
    children: [
      {
        path: 'lazypage',
        component: LazyPageComponent
      }, {
        path: 'table',
        component: TableComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule { }
