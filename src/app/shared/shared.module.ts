import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToolbarModule } from 'primeng/toolbar';
import { CardModule } from 'primeng/card';

import { NglModule } from 'ng-lightning/ng-lightning';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    InputTextModule,
    ToolbarModule,
    CardModule,
    NglModule.forRoot({
      svgPath: '/assets/icons'
    }),
    NgxDatatableModule
  ],
  declarations: [],
  exports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    InputTextModule,
    ToolbarModule,
    CardModule,
    NglModule,
    NgxDatatableModule
  ]
})
export class SharedModule { }
