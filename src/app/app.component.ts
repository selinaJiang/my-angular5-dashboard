import { Component } from '@angular/core';
import { StateService } from './core/service/state.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(public stateService: StateService) {
  }

}
