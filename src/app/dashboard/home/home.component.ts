import {Component, OnInit, ViewChild} from '@angular/core';
import { Router } from "@angular/router";
import { UIChart } from "primeng/components/chart/chart";

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit {
  data: any;
  options: any;
  myLegends: any = {};
  data2: any;
  @ViewChild('pie') pie: UIChart;
  @ViewChild('bar') bar: UIChart;

  date: Date;

  constructor(private route: Router) {
    this.data = {
      labels: ['已发送','待发送','人手发送'],
      datasets: [
        {
          data: [300, 50, 100],
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
    };
    this.options = {
      title: {
        display: true,
        text: '消息监控',
        fontSize: 16
      },
      legend: {
        display: false,
        position: 'bottom'
      },
      animation: {
        onComplete: () => {
          const data = this.pie.chart.data;
          this.myLegends = Object.assign({}, {
            values: data.datasets[0].data,
            backgroundColor: data.datasets[0].backgroundColor,
            labels: data.labels,
          })
        }
      }
    };

    this.data2 = {
      labels: ['已发送','待发送','人手发送'],
      datasets: [
        {
          label: '消息监控',
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          data: [65, 59, 80]
        }
      ]
    }
  }

  ngOnInit() {
    setInterval(() => {
      this.data.datasets[0].data = [
        Math.round(300 * (1 + Math.random())),
        Math.round(300 * (1 + Math.random())),
        Math.round(300 * (1 + Math.random()))
      ];
      this.pie.refresh();

      this.data2.datasets[0].data = [
        Math.round(300 * (1 + Math.random())),
        Math.round(300 * (1 + Math.random())),
        Math.round(300 * (1 + Math.random()))
      ];
      this.bar.refresh();
    }, 3000)

    this.date = new Date();
  }



}
