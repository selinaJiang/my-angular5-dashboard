import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule } from 'primeng/chart';
import { SharedModule } from '../shared/shared.module'

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './home/home.component'

@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [HomeComponent]
})
export class DashboardModule {}
