import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PanelMenuModule } from 'primeng/panelmenu';

import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { StateService } from './service/state.service';
import { UserService } from './service/user.service';

@NgModule({
  imports: [CommonModule, BrowserModule, BrowserAnimationsModule, PanelMenuModule],
  declarations: [HeaderComponent, MenuComponent],
  exports: [HeaderComponent, MenuComponent],
  providers: [StateService, UserService]
})
export class CoreModule {

  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

}
