import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class User {
  username: string;
}

@Injectable()
export class UserService {

  loggedIn: boolean = false;

  user: User = {
    username: 'Jiangsigui'
  };

  constructor(
    // public http: HttpClient
  ) {
    this.loggedIn = true;
  }


}
