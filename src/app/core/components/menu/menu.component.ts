import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuItem } from "primeng/components/common/menuitem";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {

  constructor() { }

  items: MenuItem[];
  collapsed: boolean = false;

  ngOnInit() {
    this.items = [
      {
        label: '首页',
        routerLink: ['/home'],
        routerLinkActiveOptions: {exact: true},
        icon: 'fa-home',
      },
      {
        label: '信息管理',
        icon: 'fa-envelope',
        items: [
          {
            label: '发送信息列表',
            routerLink: ['/lazy/table'],
          },
        ],
        command: () => { this.toggleMenu(true) }
      },
      {
        label: '信息处理',
        icon: 'fa-edit',
        items: [
          {
            label: '人手发送',
            routerLink: ['/lazy/lazypage'],
          },
          {
            label: '线下处理',
          },
        ],
        command: () => { this.toggleMenu(true) }
      },
      {
        label: '监控设置',
        icon: 'fa-desktop',
        items: [
          {
            label: '模板配置',
          },
          {
            label: '监控参数配置',
          },
          {
            label: '发送设置',
          }
        ],
        command: () => { this.toggleMenu(true) }
      },
      {
        label: '系统设置',
        icon: 'fa-gear',
        items: [
          {
            label: 'MOT系统配置',
          },
          {
            label: '发送参数配置',
          },
          {
            label: '发送负荷设置',
          },
          {
            label: '邮箱网关',
          }
        ],
        command: () => { this.toggleMenu(true) }
      }
    ];
  }

  toggleMenu(oneWay: boolean) {
    this.collapsed = oneWay ? false : !this.collapsed;
  }

}
