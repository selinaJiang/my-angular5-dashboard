import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public userService: UserService
  ) { }

  ngOnInit() {
  }

  get firstLetter() {
    const firstLetter = this.userService.user.username.substr(0, 1);
    return  firstLetter && firstLetter.toUpperCase() || '';
  }

  get loggedIn() {
    return this.userService.loggedIn;
  }

}
